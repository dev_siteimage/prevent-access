<?php


namespace App\Manager;


use App\Model\ResourcesModel;

class ShortcodesManager
{

    private $model;

    public function __construct(ResourcesModel $model)
    {
        $this->model = $model;

        $this->addShortcodes();
        add_action('init', [$this, 'replaceShortcodes']);
        //$this->replaceShortcodes();
    }

    private function addShortcodes()
    {
        add_shortcode('resources', [$this, 'resources']);
    }

    public function replaceShortcodes()
    {
        add_filter('pre_do_shortcode_tag', [$this, 'preDoShortcode'], 10, 3);
    }

    public function resources($atts)
    {
        $filters = $this->model->getFilters();
        $activeFilters = $this->model->getActiveFilters();
        $page = (get_query_var('paged')) ? get_query_var('paged') : 1;
        $perPage = 9;

        $resourcesQuery = $this->model->getResourcesQuery($activeFilters, $page, $perPage);
        $resources = $this->model->getResources($activeFilters, $page, $perPage);

        ob_start();
        get_template_part('template-parts/shortcodes/resources/resources', null, compact('filters', 'resourcesQuery', 'page', 'activeFilters'));
        return ob_get_clean();
    }

    public function preDoShortcode($return, $tag, $atts)
    {
        if ($tag === 'smlsubform') {
            $return = $this->smlsubform($atts);
        }
        return $return;
    }

    public function smlsubform($atts = [])
    {
        $shortcodeatts = shortcode_atts(array(
            "prepend" => '',
            "showname" => true,
            "nametxt" => 'Name:',
            "firstnametxt" => 'First Name:',
            "lastnametxt" => 'Last Name:',
            "nameholder" => 'Name...',
            "firstnameholder" => 'First Name...',
            "lastnameholder" => 'Last Name...',
            "emailtxt" => 'Email:',
            "emailholder" => 'Email Address...',
            "showsubmit" => true,
            "submittxt" => 'Submit',
            "jsthanks" => false,
            "thankyou" => 'Thank you for subscribing to our mailing list'
        ), $atts);

        ob_start();
        get_template_part('template-parts/shortcodes/smlsubform.php', null, $shortcodeatts);
        return ob_get_clean();
    }

}
