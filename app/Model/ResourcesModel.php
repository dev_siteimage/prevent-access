<?php

namespace App\Model;

class ResourcesModel
{

    /**
     * @var \WP_Taxonomy[]
     */
    private $taxonomies;

    private $filters;

    private $activeFilters;

    public function __construct()
    {

    }

    public function getResources($filters, $page = 1, $perPage = 10)
    {
        return $this->getResourcesQuery($filters, $page, $perPage)->get_posts();
    }

    public function getResourcesQuery($filters, $page = 1, $perPage = 10)
    {
        $limit = $perPage > 0 ? $perPage : 1;
        $offset = $page > 1 ? ($page - 1) * $limit : 0;

        $args = [
            'post_type' => 'resource',
            'limit' => $limit,
            'offset' => $offset,
            'posts_per_page' => $perPage,
            'paged' => $page
        ];

        if (!empty($filters)) {
            $taxQuery = [];
            foreach ($this->getTaxonomies() as $taxonomyName => $taxonomy) {
                if (array_key_exists($taxonomyName, $filters)) {
                    $taxQuery[] = [
                        'taxonomy' => $taxonomyName,
                        'terms' => $filters[$taxonomyName],
                        'field' => 'slug',
                        'operator' => 'IN',
                    ];
                }
            }
            if (!empty($taxQuery)) {
                $args['tax_query'] = $taxQuery;
            }
        }

        $dates = [];
        if (array_key_exists('start_date', $filters)) {
            $dates['after'] = $filters['start_date'];
        }
        if (array_key_exists('end_date', $filters)) {
            $dates['before'] = $filters['end_date'];
        }

        if (!empty($dates)) {
            $dates['inclusive'] = true;
            $args['date_query'] = $dates;
        }

        return new \WP_Query($args);
    }

    public function getTaxonomies()
    {
        if (!isset($this->taxonomies)) {
            $this->taxonomies = [];
            foreach (get_object_taxonomies('resource') as $taxonomy) {
                $this->taxonomies[$taxonomy] = get_taxonomy($taxonomy);
            }
        }
        return $this->taxonomies;
    }

    public function getFilters()
    {
        if (!isset($filters)) {
            $filters = [
                'start_date' => [
                    'name' => 'start_date',
                    'label' => 'Start date',
                    'type' => 'date',
                    'value' => array_key_exists('start_date', $_GET) ? $_GET['start_date'] : null,
                ],
                'end_date' => [
                    'name' => 'end_date',
                    'label' => 'End date',
                    'type' => 'date',
                    'value' => array_key_exists('end_date', $_GET) ? $_GET['end_date'] : null,
                ],
            ];
            foreach ($this->getTaxonomies() as $taxonomyName => $taxonomy) {
                $filter = [
                    'name' => $taxonomyName,
                    'label' => $taxonomy->label
                ];
                if ($taxonomyName === 'keywords') {
                    $filter['type'] = 'text';
                } else {
                    $filter['type'] = 'radiogroup';
                    $options = [];
                    foreach (get_terms(['taxonomy' => $taxonomyName, 'hide_emty' => true]) as $term) {
                        $options[$term->slug] = $term->name;
                    }
                    $filter['options'] = $options;
                }

                $filter['value'] = array_key_exists($taxonomyName, $_GET) ? $_GET[$taxonomyName] : null;

                $filters[$taxonomyName] = $filter;
            }

            $this->filters = $filters;
        }

        return $this->filters;
    }

    public function getActiveFilters()
    {
        if (!isset($this->activeFilters)) {
            $this->activeFilters = [];
            foreach ($this->getFilters() as $filterName => $filter) {
                if (!empty($filter['value'])) {
                    $this->activeFilters[$filterName] = $filter['value'];
                }
            }
        }
        return $this->activeFilters;
    }

}
