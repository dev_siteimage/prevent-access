<?php

namespace App\Cpt;

use App\Manager\ShortcodesManager;
use App\Model\ResourcesModel;

class CptManager
{

    private $model;

    private $shortcodesManager;

    public function __construct()
    {
        if (did_action('init') || doing_action('init')) {
            $this->init();
        } else {
            $this->addHooks();
        }
    }

    private function addHooks()
    {
        add_action('init', [$this, 'init']);
    }

    public function init()
    {
        $this->model = new ResourcesModel();
        $this->shortcodesManager = new ShortcodesManager($this->model);
        $this->registerPostTypes();
    }

    public function registerPostTypes()
    {
        // Resource
        register_post_type(
            'resource',
            [
                'labels' => [
                    'name' => __( 'Resources', 'prevent-access' ),
                    'singular_name' => __( 'Resource', 'prevent-access' ),
                    'add_new' => __( 'Add New' , 'prevent-access' ),
                    'add_new_item' => __( 'Add New Resource' , 'prevent-access' ),
                    'edit_item' => __( 'Edit Resource' , 'prevent-access' ),
                    'new_item' => __( 'New Resource' , 'prevent-access' ),
                    'view_item' => __( 'View Resource', 'prevent-access' ),
                    'search_items' => __( 'Search Resources', 'prevent-access' ),
                    'not_found' => __( 'No Resources found', 'prevent-access' ),
                    'not_found_in_trash' => __( 'No Resources found in Trash', 'prevent-access' ),
                ],
                'public' => true,
                'has_archive' => false,
                'supports' => [
                    'title',
                    'editor',
                    'excerpt',
                    'thumbnail'
                ],
                //'rewrite' => ['slug' => 'resource'],
            ]
        );

        register_taxonomy(
            'keywords',
            'resource',
            [
                'hierarchical' => false,
                'labels' => [
                    'name' => __( 'Keywords'),
                    'singular_name' => __( 'Keyword'),
                    'search_items' =>  __( 'Search Keywords' ),
                    'popular_items' => __( 'Popular Keywords' ),
                    'all_items' => __( 'All Keywords' ),
                    'parent_item' => null,
                    'parent_item_colon' => null,
                    'edit_item' => __( 'Edit Keyword' ),
                    'update_item' => __( 'Update Keyword' ),
                    'add_new_item' => __( 'Add New Keyword' ),
                    'new_item_name' => __( 'New Keyword Name' ),
                    'separate_items_with_commas' => __( 'Separate keywords with commas' ),
                    'add_or_remove_items' => __( 'Add or remove keywords' ),
                    'choose_from_most_used' => __( 'Choose from the most used keywords' ),
                    'menu_name' => __( 'Keywords' ),
                ],
                'show_ui' => true,
                'show_in_rest' => true,
                'show_admin_column' => true,
                'update_count_callback' => '_update_post_term_count',
                'query_var' => true,
                //'rewrite' => array( 'slug' => 'keyword' ),
            ]
        );

        register_taxonomy(
            'country',
            'resource',
            [
                'hierarchical' => false,
                'labels' => [
                    'name' => __( 'Countries'),
                    'singular_name' => __( 'Country'),
                    'search_items' =>  __( 'Search Countries' ),
                    'popular_items' => __( 'Popular Countries' ),
                    'all_items' => __( 'All Countries' ),
                    'parent_item' => null,
                    'parent_item_colon' => null,
                    'edit_item' => __( 'Edit Country' ),
                    'update_item' => __( 'Update Country' ),
                    'add_new_item' => __( 'Add New Country' ),
                    'new_item_name' => __( 'New Country Name' ),
                    'separate_items_with_commas' => __( 'Separate countries with commas' ),
                    'add_or_remove_items' => __( 'Add or remove countries' ),
                    'choose_from_most_used' => __( 'Choose from the most used countries' ),
                    'menu_name' => __( 'Countries' ),
                ],
                'show_ui' => true,
                'show_in_rest' => true,
                'show_admin_column' => true,
                'update_count_callback' => '_update_post_term_count',
                'query_var' => true,
                //'rewrite' => array( 'slug' => 'country' ),
            ]
        );

        register_taxonomy(
            'resource-type',
            'resource',
            [
                'hierarchical' => false,
                'labels' => [
                    'name' => __( 'Resource types'),
                    'singular_name' => __( 'Resource type'),
                    'search_items' =>  __( 'Search Resource types' ),
                    'popular_items' => __( 'Popular Resource types' ),
                    'all_items' => __( 'All Resource types' ),
                    'parent_item' => null,
                    'parent_item_colon' => null,
                    'edit_item' => __( 'Edit Resource type' ),
                    'update_item' => __( 'Update Resource type' ),
                    'add_new_item' => __( 'Add New Resource type' ),
                    'new_item_name' => __( 'New Resource type Name' ),
                    'separate_items_with_commas' => __( 'Separate resource types with commas' ),
                    'add_or_remove_items' => __( 'Add or remove resource types' ),
                    'choose_from_most_used' => __( 'Choose from the most used resource types' ),
                    'menu_name' => __( 'Resource types' ),
                ],
                'show_ui' => true,
                'show_in_rest' => true,
                'show_admin_column' => true,
                'update_count_callback' => '_update_post_term_count',
                'query_var' => true,
                //'rewrite' => array( 'slug' => 'resource-type' ),
            ]
        );


        // Teams
        register_post_type(
            'team-member',
            [
                'labels' => [
                    'name' => __( 'Team members', 'prevent-access' ),
                    'singular_name' => __( 'Team member', 'prevent-access' ),
                    'add_new' => __( 'Add New' , 'prevent-access' ),
                    'add_new_item' => __( 'Add New Team member' , 'prevent-access' ),
                    'edit_item' => __( 'Edit Team member' , 'prevent-access' ),
                    'new_item' => __( 'New Team member' , 'prevent-access' ),
                    'view_item' => __( 'View Team member', 'prevent-access' ),
                    'search_items' => __( 'Search Team members', 'prevent-access' ),
                    'not_found' => __( 'No Team members found', 'prevent-access' ),
                    'not_found_in_trash' => __( 'No Team members found in Trash', 'prevent-access' ),
                ],
                'public' => true,
                'has_archive' => false,
                'supports' => [
                    'title',
                    'editor',
                    'excerpt',
                    'thumbnail'
                ],
                //'rewrite' => ['slug' => 'team-member'],
            ]
        );
        register_taxonomy(
            'location',
            'team-member',
            [
                'hierarchical' => false,
                'labels' => [
                    'name' => __( 'Locations', 'prevent-access' ),
                    'singular_name' => __( 'Location', 'prevent-access' ),
                    'search_items' =>  __( 'Search Locations', 'prevent-access' ),
                    'popular_items' => __( 'Popular Locations', 'prevent-access' ),
                    'all_items' => __( 'All Locations', 'prevent-access' ),
                    'parent_item' => null,
                    'parent_item_colon' => null,
                    'edit_item' => __( 'Edit Location', 'prevent-access' ),
                    'update_item' => __( 'Update Location', 'prevent-access' ),
                    'add_new_item' => __( 'Add New Location', 'prevent-access' ),
                    'new_item_name' => __( 'New Location Name', 'prevent-access' ),
                    'separate_items_with_commas' => __( 'Separate locations with commas', 'prevent-access' ),
                    'add_or_remove_items' => __( 'Add or remove locations', 'prevent-access' ),
                    'choose_from_most_used' => __( 'Choose from the most used locations', 'prevent-access' ),
                    'menu_name' => __( 'Locations', 'prevent-access' ),
                ],
                'show_ui' => true,
                'show_in_rest' => true,
                'show_admin_column' => true,
                'update_count_callback' => '_update_post_term_count',
                'query_var' => true,
                //'rewrite' => array( 'slug' => 'location' ),
            ]
        );
        register_taxonomy(
            'role',
            'team-member',
            [
                'hierarchical' => false,
                'labels' => [
                    'name' => __( 'Roles', 'prevent-access' ),
                    'singular_name' => __( 'Role', 'prevent-access' ),
                    'search_items' =>  __( 'Search Roles', 'prevent-access' ),
                    'popular_items' => __( 'Popular Roles', 'prevent-access' ),
                    'all_items' => __( 'All Roles', 'prevent-access' ),
                    'parent_item' => null,
                    'parent_item_colon' => null,
                    'edit_item' => __( 'Edit Role', 'prevent-access' ),
                    'update_item' => __( 'Update Role', 'prevent-access' ),
                    'add_new_item' => __( 'Add New Role', 'prevent-access' ),
                    'new_item_name' => __( 'New Role Name', 'prevent-access' ),
                    'separate_items_with_commas' => __( 'Separate roles with commas', 'prevent-access' ),
                    'add_or_remove_items' => __( 'Add or remove roles', 'prevent-access' ),
                    'choose_from_most_used' => __( 'Choose from the most used roles', 'prevent-access' ),
                    'menu_name' => __( 'Roles', 'prevent-access' ),
                ],
                'show_ui' => true,
                'show_in_rest' => true,
                'show_admin_column' => true,
                'update_count_callback' => '_update_post_term_count',
                'query_var' => true,
                //'rewrite' => array( 'slug' => 'role' ),
            ]
        );
    }

}
