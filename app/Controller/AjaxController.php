<?php


namespace App\Controller;


class AjaxController
{

    public function __construct()
    {
        $this->registerEndpoints();
    }

    private function registerEndpoints()
    {
        add_action('wp_ajax_process_community_form', [$this, 'processCommunityForm']);
        add_action('wp_ajax_nopriv_process_community_form', [$this, 'processCommunityForm']);
    }

    public function processCommunityForm()
    {
        $formData = filter_input_array(INPUT_POST);
        $success = true;

        if ($success) {
            wp_send_json_success($formData);
        } else {
            wp_send_json_error($formData);
        }
    }

}
