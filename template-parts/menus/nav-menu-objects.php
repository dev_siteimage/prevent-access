<?php

if (!defined('ABSPATH') || !isset($args)) {
    die;
}

extract($args);

/**
 * @var string $icon
 * @var array $menu
 */

?>
<div class="menu-dropdown">
    <div class="container">
        <ul class="dropdown-list">
            <li class="dropdown-list__item">
                <img src="<?php echo $icon; ?>" />
            </li>
            <?php foreach ((array) $menu as $menuItem) : ?>
                <li class="dropdown-list__item">
                    <a class="dropdown-list__link" href="<?php echo $menuItem['menu_item_link']; ?>">
                        <span class="dropdown-list__title"><?php echo $menuItem['menu_item_title']; ?>
                            <svg width="12" height="7" viewBox="0 0 12 7" fill="none" xmlns="http://www.w3.org/2000/svg">
                            <path fill-rule="evenodd" clip-rule="evenodd"
                                d="M1.78333 0.666687L6 4.50002L10.2167 0.666687L11.5 1.83335L6 6.83335L0.5 1.83335L1.78333 0.666687Z"
                                fill="#979797"/>
                            </svg>
                        </span>
                        <p class="dropdown-list__descr"><?php echo $menuItem['menu_item_descr']; ?></p>
                        <img class="dropdown-list__subimg" src="<?php echo $menuItem['menu_item_subimage']; ?>"
                             alt="<?php echo $menuItem['menu_item_title']; ?>"/>
                    </a>
                </li>
            <?php endforeach; ?>
        </ul>
    </div>
</div>
