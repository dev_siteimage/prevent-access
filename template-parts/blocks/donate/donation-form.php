<section class="donation-form">
  <div class="container">
    <div class="donation-form__wrapper">
      <div class="donation-form__info">
        <img  class="donation-form__img" src="<?php echo get_field('donation_form_image') ?>" alt="<?php echo get_field('donation_form_title') ?>">
        <div class="donation-form__info-content">
          <h1 class="donation-form__title"><?php titleColor(get_field('donation_form_title'), "1") ?></h1>
          <p class="donation-form__text"><?php echo get_field('donation_form_text') ?></p>
        </div>
      </div>
      <div class="donation-form__description">
        <?php echo do_shortcode('[give_form id="365"]') ?>
      </div>
    </div>
  </div>
</section>