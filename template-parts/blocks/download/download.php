<div class="section section-download">
  <div class="container">
    <div class="section-download__wrapper">
      <div class="section-download__img">
        <img src="<?php echo get_field('download_image')  ?>" alt="<?php echo get_field('download_image_text')?>">
      </div>
      <ul class="download-list">
        <?php $fields = get_field('download_item'); ?>
          <?php foreach($fields as $field) :?>
            <li class="download-list__item">
              <strong class="download-list__title"><?php echo $field['download_item_title']?></strong>
              <a href="<?php echo $field['download_item_button_link']?>" download class="btn"><?php echo $field['download_item_button_text']?></a>
            </li>
          <?php endforeach; ?>
      </ul>
    </div>
  </div>
</div>