<div class="section section-single-map">
  <div class="container">
    <iframe class="section-map__iframe"  src="<?php echo get_field('single_map')?>"></iframe>
  </div>
</div>