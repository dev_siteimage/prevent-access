<section class="section section-form" id="aplication-form">
  <img src="<?php echo get_field('application_form_image_one') ?>" alt="<?php echo get_field('application_form_title') ?>" class="section-form__img section-form__img--one">
  <img src="<?php echo get_field('application_form_image_two') ?>" alt="<?php echo get_field('application_form_title') ?>" class="section-form__img section-form__img--two">
  <div class="container">
    <div class="section-form__content">
      <h2 class="section-title  section-title--gold"><?php titleColor(get_field('application_form_title'), "0") ?></h2>
      <form class="aplication-form">
        <div class="aplication-form__item aplication-form__item--50">
          <label for="" class="aplication-form__label require"><?php _e('First Name', 'prevent-access') ?></label>
          <input class="aplication-form__input" type="text" name="firstname" id="" required>
        </div>
        <div class="aplication-form__item aplication-form__item--50">
          <label for="" class="aplication-form__label require"><?php _e('Last Name', 'prevent-access') ?></label>
          <input class="aplication-form__input" type="text" name="lastname" id="" required>
        </div>
        <div class="aplication-form__item">
          <label for="" class="aplication-form__label require"><?php _e('Organization Name', 'prevent-access') ?></label>
          <input class="aplication-form__input" type="text" name="orgname" id="" required>
        </div>
        <div class="aplication-form__item aplication-form__item--50">
          <label for="" class="aplication-form__label require"><?php _e('E-mail Address', 'prevent-access') ?></label>
          <input class="aplication-form__input" type="email" name="email" id="" required>
        </div>
        <div class="aplication-form__item aplication-form__item--50">
          <label for="" class="aplication-form__label require"><?php _e('Phone', 'prevent-access') ?></label>
          <input class="aplication-form__input" type="tel" name="phone" id="" required>
        </div>
        <div class="aplication-form__item">
          <label for="" class="aplication-form__label require"><?php _e('Street Address', 'prevent-access') ?></label>
          <input class="aplication-form__input" type="text" name="saddres" id="" required>
        </div>
        <div class="aplication-form__item aplication-form__item--50">
          <label for="" class="aplication-form__label require"><?php _e('City', 'prevent-access') ?></label>
          <input class="aplication-form__input" type="text" name="city" id="" required>
        </div>
        <div class="aplication-form__item aplication-form__item--50">
          <label for="" class="aplication-form__label require"><?php _e('State', 'prevent-access') ?></label>
          <input class="aplication-form__input" type="text" name="state" id="" required>
        </div>
        <div class="aplication-form__item aplication-form__item--50">
          <label for="" class="aplication-form__label require"><?php _e('Zipcode', 'prevent-access') ?></label>
          <input class="aplication-form__input" type="number" name="zipcode" id="" required>
        </div>
        <div class="aplication-form__item aplication-form__item--50">
          <label for="" class="aplication-form__label require"><?php _e('Country', 'prevent-access') ?></label>
          <input class="aplication-form__input" id="country" type="text" name="country" id="" required>
        </div>
        <div class="aplication-form__item">
          <label for="" class="aplication-form__label require"><?php _e('Website', 'prevent-access') ?></label>
          <input class="aplication-form__input" type="url" name="website" id="" require>
        </div>
        <div class="aplication-form__item">
          <span class="aplication-form__label"><?php _e('Are you okay with being recognized for your sign-on on social media?', 'prevent-access') ?></span>
          <div class="aplication-form__item-wrap">
            <label class="s-input">
              <input type="checkbox" name="" id="" class="hidden">
              <span  class="s-input__icon s-input__icon--radio"></span>
              <span  class="s-input__text"><?php _e('Yes', 'prevent-access') ?></span>
            </label>
            <label class="s-input">
              <input type="checkbox" name="" id="" class="hidden">
              <span  class="s-input__icon s-input__icon--radio"></span>
              <span  class="s-input__text"><?php _e('No', 'prevent-access') ?></span>
            </label>
  
          </div>
          
        </div>
        <div class="aplication-form__item">
          <label for="" class="aplication-form__label require"><?php _e('Message', 'prevent-access') ?></label>
          <textarea class="aplication-form__textarea" name="message" id="" cols="30" rows="10"></textarea>
        </div>
        <div class="aplication-form__item">
          <label class="s-input">
            <input type="checkbox" name="" id="" class="hidden">
            <span  class="s-input__icon"></span>
            <span  class="s-input__text"><?php _e('I am authorized to endorse on behalf of this organization.', 'prevent-access') ?></span>
          </label>
        </div>
        <div class="aplication-form__button">
          <button class="btn btn--large" type="submit" data-contact-form><?php _e('Submit', 'prevent-access') ?></button>
        </div>

        <div class="aplication-form__overlay" data-form-overlay>
          <div class="aplication-form__spin"></div>
        </div>
  
      </form>
    </div>
  </div>
</section>