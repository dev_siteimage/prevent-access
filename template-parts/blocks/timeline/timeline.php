<section class="section section-timeline">
  <div class="container">
    <h2 class="section-timeline__title"><?php echo get_field('timeline_top_title')?> </h2>
    <ul class="timeline">

    <?php $fields = get_field('timeline_item'); ?>

    <?php foreach($fields as $field) :?>

      <li class="timeline__item">
        <div class="timeline__element"></div>
        <div class="timeline__center">
          <div class="timeline__center-circle"></div>
          <span class="timeline__center-year"><?php echo $field['timeline_item_year']; ?> </span>
        </div>
        <div class="timeline__element" data-timeline>
          <div class="timeline__element-wrapper">
            <div class="timeline-item">
              <time class="timeline-item__date"><?php echo $field['timeline_item_date']; ?></time>
              <h3 class="timeline-item__title"><?php echo $field['timeline_item_title']; ?></h3>
              <p class="timeline-item__text"><?php echo $field['timeline_item_text']; ?></p>
              <?php if($field['timeline_item_image']) { ?>
                <img class="timeline-item__img" src="<?php echo $field['timeline_item_image']; ?>" alt="<?php echo $field['timeline_item_title']; ?>">
              <?php } ?>
              <?php if($field['timeline_item__link_text']) { ?>
                <a href="<?php echo $field['timeline_item_url']; ?>" class="timeline-item__link" download><?php echo $field['timeline_item__link_text']; ?> <?php echo pac_svg('chevron'); ?></a>
              <?php } ?>
            </div>
          </div>
        </div>
      </li>

    <?php endforeach; ?>

    </ul>
    <h2 class="section-timeline__title"><?php echo get_field('timeline_bottom_title')?> </h2>
  </div>
</section>