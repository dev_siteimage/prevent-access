<section class="section section-advantages  <?php if(get_field('advantages_descr')) { ?> section-advantages--yellow <?php } ?>">
  <div class="container">
    <?php if(get_field('advantages_descr')) { ?>
      <div class="section-advantages__top">
        <h2 class="section-advantages__title"><?php echo get_field('advantages_title') ?></h2>
        <p class="section-advantages__descr"><?php echo get_field('advantages_descr') ?></p>
      </div>
      <?php } else { ?>
        <h2 class="section-advantages__title section-advantages__title--center"><?php echo get_field('advantages_title') ?></h2>
        <?php } ?>
    <ul class="advantages">
    <?php $fields = get_field('advantages_item'); ?>
      <?php foreach($fields as $field) :?>
        <li class="advantages__item">
          <span class="advantages__number"><?php echo $field['advantages_item_number']; ?></span>
          <strong class="advantages__text"><?php echo $field['advantages_item_text']; ?></strong>
          <?php if($field['advantages_item_descr']) { ?>
            <p class="advantages__descr"><?php echo $field['advantages_item_descr']; ?></p>
          <?php } ?>
        </li>
        <?php endforeach; ?>
    </ul>
  </div>
</section>