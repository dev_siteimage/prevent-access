<section class="section section-introduction">
  <div class="container">
    <h2 class="section-introduction__title"><?php echo get_field('introduction_title') ?></h2>
    <?php $string = get_field('introduction_text'); ?>
    <p class="section-introduction__text"><?php echo preg_replace('#(\d+)#', '<span>$1</span>', $string); ?></p>
  </div>
</section>