<section class="section section-blog-slider section-bordered " data-slider>
    <div class="container">
        <div class="section-blog-slider__top">
          <h2 class="section-blog-slider__title"><?php echo get_field('articles_slider_title') ?></h2>
          <div class="section-blog-slider__buttons">
            <button class="section-blog-slider__button section-blog-slider__button--prev" type="button" data-articles-slider-prev><?php echo pac_svg('chevron'); ?></button>
            <button class="section-blog-slider__button section-blog-slider__button--next" type="button" data-articles-slider-next><?php echo pac_svg('chevron'); ?></button>
          </div>
        </div>
        <?php
        $article_posts = get_field('articles_slider_item');
        if( $article_posts ): ?>
            <ul class="blog-articles blog-articles--slider" data-articles-slider>
                <?php foreach( $article_posts as $article_post ): 
                    $permalink = get_permalink( $article_post->ID );
                    $descr = get_the_excerpt( $article_post->ID );
                    $title = get_the_title( $article_post->ID );
                    $img = get_the_post_thumbnail_url( $article_post->ID );
                    $authorName = get_the_author_firstname($article_post->ID );
                    $authorLastName = get_the_author_lastname($article_post->ID );
                    $authorImg = get_avatar_url($article_post->ID );
                    ?>
                    <li class="blog-articles__item">
                        <div class="blog-articles__img">
                            <img src="<?php echo $img; ?>" alt="<?php echo $title; ?>">
                        </div>
                        <div class="blog-articles__content">
                            <h3 class="blog-articles__title"><?php echo $title; ?></h3>
                            <div class="blog-articles__author">
                                <div class="blog-articles__author-img">
                                    <img src="<?php echo $authorImg ?>" alt="<?php echo $authorName; _e(' '); echo $authorLastName; ?>">
                                </div>
                                <span class="blog-articles__author-name"><?php echo $authorName; _e(' '); echo $authorLastName; ?></span>
                            </div>
                            <?php if($descr) { ?>
                                <p class="blog-articles__description blog-articles__description--short"><?php echo $descr; ?></p>
                           <?php  } ?>
                            <a class="blog-articles__link" href="<?php echo esc_url( $permalink ); ?>"><?php _e('Read more >', 'pac') ?></a>
                        </div>
                    </li>
                <?php endforeach; ?>
            </ul>
        <?php endif; ?>

    </div>
</section>