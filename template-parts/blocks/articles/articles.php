<section class="section section-article section-bordered">
    <div class="container">
        <div class="section-blog-slider__top">
          <h2 class="section-blog-slider__title"><?php echo get_field('article_section_title') ?></h2>
          <div class="section-blog-slider__buttons">
            <button class="section-blog-slider__button section-blog-slider__button--prev" type="button" data-articles-slider-prev><?php echo pac_svg('chevron'); ?></button>
            <button class="section-blog-slider__button section-blog-slider__button--next" type="button" data-articles-slider-next><?php echo pac_svg('chevron'); ?></button>
          </div>
        </div>
        <?php
        $article_posts = get_field('article_item');
        if( $article_posts ): ?>
            <ul class="article-post" data-articles-slider>
                <?php foreach( $article_posts as $article_post ): 
                    $permalink = get_permalink( $article_post->ID );
                    $title = get_the_title( $article_post->ID );
                    $img = get_the_post_thumbnail_url( $article_post->ID );
                    ?>
                    <li class="article-post__item">
                        <a class="article-post__link" href="<?php echo esc_url( $permalink ); ?>">
                            <img src="<?php echo $img; ?>" alt="<?php echo $title; ?>">
                            <h3 class="article-post__title"><?php echo $title; ?></h3>
                        </a>
                    </li>
                <?php endforeach; ?>
            </ul>
        <?php endif; ?>

    </div>
</section>