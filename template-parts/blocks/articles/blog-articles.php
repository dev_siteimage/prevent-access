<section class="section section-blog">
    <div class="container">
        <?php
        $article_posts = get_field('blog_articles_item');
        if( $article_posts ): ?>
            <ul class="blog-articles blog-articles--2">
                <?php foreach( $article_posts as $article_post ): 
                    $permalink = $article_post['blog_articles_item_link_url'];
                    $descr = $article_post['blog_articles_item_description'];
                    $date = $article_post['blog_articles_item_date'];
                    $title = $article_post['blog_articles_item_title'];
                    $img = $article_post['blog_articles_item_image'];
                    $authorName = $article_post['blog_articles_item_name'];;
                    $authorImg = $article_post['blog_articles_item_author_image'];
                    $buttonText = $article_post['blog_articles_item_link_text'];
                    ?>
                    <li class="blog-articles__item">
                        <div class="blog-articles__img">
                            <img src="<?php echo $img; ?>" alt="<?php echo $title; ?>">
                        </div>
                        <div class="blog-articles__content">
                            <time class="blog-articles__date"><?php echo $date; ?></time>
                            <h3 class="blog-articles__title"><?php echo $title; ?></h3>
                            <div class="blog-articles__author">
                                <div class="blog-articles__author-img">
                                    <img src="<?php echo $authorImg ?>" alt="<?php echo $authorName; ?>">
                                </div>
                                <span class="blog-articles__author-name"><?php echo $authorName;  ?></span>
                            </div>
                            <?php if($descr) { ?>
                                <p class="blog-articles__description"><?php echo $descr; ?></p>
                           <?php  } ?>
                            <a class="blog-articles__link" href="<?php echo esc_url( $permalink ); ?>"><?php echo $buttonText ?></a>
                        </div>
                    </li>
                <?php endforeach; ?>
            </ul>
        <?php endif; ?>

    </div>
</section>