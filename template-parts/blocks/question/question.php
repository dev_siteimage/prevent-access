<section class="section section-question">
  <div class="container">
    <?php $fields = get_field('question'); ?>

    <ul class="accordion-list">

      <?php foreach($fields as $field) : 
          if ($field['question_title']) : ?>
            <li class="accordion-list__item" data-accordion>
              <h2 class="accordion-list__title" data-accordion-button><span><?php echo $field['question_title']; ?></span><?php echo pac_svg('chevron'); ?> </h2>
              <p class="accordion-list__text" data-accordion-content><?php echo $field['question_answer']; ?></p>
            </li>
         <?php endif; endforeach; ?>
     </ul>

  </div>
</section>