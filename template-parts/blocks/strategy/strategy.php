<section class="section section-strategy section-bordered">
  <div class="container">
    <div class="section-advantages__top">
      <h2 class="section-advantages__title"><?php echo get_field('strategy_title') ?></h2>
      <p class="section-advantages__descr"><?php echo get_field('strategy_descr') ?></p>
    </div>
    <?php 
    $article_posts = get_field('strategy_item');
        if( $article_posts ): ?>
            <ul class="strategy">
                <?php foreach( $article_posts as $article_post ): 
                    $permalink = get_permalink( $article_post->ID );
                    $descr = get_the_excerpt( $article_post->ID );
                    $title = get_the_title( $article_post->ID );
                    $img = get_the_post_thumbnail_url( $article_post->ID );
                    ?>
                    <li class="strategy__item">
                        <a class="strategy__link" href="<?php echo esc_url( $permalink ); ?>">
                          <div class="strategy__img">
                            <img src="<?php echo $img; ?>" alt="<?php echo $title; ?>">
                          </div>
                            <div class="strategy__content">
                              <h3 class="strategy__title"><?php echo $title; ?></h3>
                              <?php if($descr) { ?>
                                <p class="strategy__descr"><?php echo $descr; ?></p>
                              <?php } ?>
                            </div>
                        </a>
                    </li>
                <?php endforeach; ?>
            </ul>
        <?php endif; ?>
  </div>
</section>