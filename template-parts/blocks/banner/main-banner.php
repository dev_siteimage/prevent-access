<section class="section section-video">
    <div class="section-video__wrapper">
      <div class="section-video__content-wrapp">
        <div class="section-video__content">
          <div class="section-video__content-inner">
            <h1 class="section-video__title"><?php echo get_field('main_video_title')?></h1>
            <strong class="section-video__subtitle"><?php echo get_field('main_video_subtitle')?></strong>
            <p class="section-video__text"><?php echo get_field('main_video_text')?></p>
            <a href="<?php echo get_permalink( get_page_by_path( 'community' ) ); ?>" class="btn btn--large"><?php echo get_field('main_video_button_text')?></a>
            <p class="section-video__descr"><span><?php echo get_field('main_video_description_blue')?></span><?php echo get_field('main_video_description')?></p>
            <a href="<?php echo get_permalink( get_page_by_path( 'community' ) ); ?>" class="btn btn--large"><?php echo get_field('main_video_button_text')?></a>
          </div>
        </div>   
      </div>
      <?php $file = get_field('main_video_file') ?>
      <div class="section-video__video " data-video-container>
        <video id="site-video" autoplay="" muted="" loop="" playsinline="" aria-label="Video" data-video>
          <source src="<?php echo $file['url']  ?>" type="video/mp4">
        </video>
        <button class="btn-video play" data-video-button>
          <span></span>
          <span></span>
        </button>
      </div>
    </div>
</section>
