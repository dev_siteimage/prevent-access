<div class="photo-banner">
  <img src="<?php echo get_field('photo_banner_image') ?>" alt="<?php echo get_field('photo_banner_text') ?>" class="photo-banner__img">
</div>