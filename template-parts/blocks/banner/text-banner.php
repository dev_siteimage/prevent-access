<section class="section section-text-banner section-text-banner--<?php echo get_field('text_banner_color')?>">
    <div class="container">
      <h1 class="section-text-banner__title"><?php echo get_field('text_banner_title')?><span><?php echo get_field('text_banner_title_colorful')?></span></h1>
      <?php if(get_field('text_banner_descr')) { ?>
        <p class="section-text-banner__text"><?php echo get_field('text_banner_descr')?></p>
      <?php } ?>
    </div>
</section>
