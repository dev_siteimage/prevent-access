<section class="section-shop">
  <div class="container">
    <h2 class="section-title section-title--purple"><?php titleColor(get_field('shop_title'), "1") ?></h2>
    <ul class="shop-list">
      <?php $fields = get_field('shop_item'); ?>
        <?php foreach($fields as $field) :?>
          <li class="shop-list__item">
              <a href="<?php echo $field['shop_item_link'] ?>" class="shop-list__link">
                <img src="<?php echo $field['shop_item_image'] ?>" alt="<?php echo $field['shop_item_title'] ?>" class="shop-list__img">
                <div class="shop-list__content">
                  <strong class="shop-list__title"><?php echo $field['shop_item_title'] ?></strong>
                  <span class="shop-list__price"><?php echo $field['shop_item_price'] ?></span>
                </div>
              </a>
          </li>
          <?php endforeach; ?>
      </ul>
  </div>
</section>