<section class="section section-location">
  <div class="container">
    <h2 class="section-title section-title--blue"><?php titleColor(get_field('location_title'), "0") ?></h2>
    <iframe class="section-map__iframe"  src="<?php echo get_field('location_map_url')?>"></iframe>
    <div class="section-location__buttons">
      <button type="button" class="btn" data-scroll-form><?php echo get_field('location_second_button_text')?></button>
    </div>
  </div>
</section>