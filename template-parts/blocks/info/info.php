<div class="section section-info">
  <div class="container">
    <?php $fields = get_field('info_item'); ?>

    <ul class="info">

      <?php foreach($fields as $field) : 
          if ($field['info_item_icon']) : ?>
            <li class="info__item">
              <div class="info__item-icon"><?php echo file_get_contents($field['info_item_icon']); ?></div>
              <span class="info__item-title"><?php echo $field['info_item_title']; ?></span>
              <span class="info__item-text"><?php echo $field['info_item_text']; ?></span>
            </li>
         <?php endif; endforeach; ?>
     </ul>
  </div>
</div>