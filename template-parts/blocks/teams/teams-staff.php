<section class="section-teams">
  <div class="container">
    <div class="section-teams__top">
      <h2 class="section-teams__title"><?php echo get_field('teams_staff_title') ?></h2>
      <p class="section-teams__descr"><?php echo get_field('teams_staff_descr') ?></p>
    </div>
    <?php
        $article_posts = get_field('teams_staff_item');
        if( $article_posts ): ?>
            <ul class="teams-staff">
                <?php foreach( $article_posts as $article_post ): 
                    $permalink = get_permalink( $article_post->ID );
                    $descr = get_the_excerpt( $article_post->ID );
                    $title = get_the_title( $article_post->ID );
                    $img = get_the_post_thumbnail_url( $article_post->ID );
                    $termsRole = get_the_terms($article_post->ID, 'role');
                    $termsLocation = get_the_terms($article_post->ID, 'location');
                    ?>
                    <li class="teams-staff__item">
                      <a href="<?php echo $permalink ?>" class="teams-staff__link">
                        <div class="teams-staff__img">
                          <img src="<?php echo $img ?>" alt="<?php echo $title ?>">
                        </div>
                        <div class="teams-staff__content">
                          <h3 class="teams-staff__title"><?php echo $title ?></h3>
                          <span class="teams-staff__tag">
                            <?php 
                              if( is_array( $termsRole ) ){
                                foreach( $termsRole as $cur_term ){
                                  echo '<span>'. $cur_term->name .'</span>/';
                                }
                              }
                              if( is_array( $termsLocation ) ){
                                foreach( $termsLocation as $cur_loc ){
                                  echo '<span>'. $cur_loc->name .'</span>';
                                }
                              }
                             ?>
                          </span>
                        </div>
                        <?php if($descr) { ?>
                          <p class="teams-staff__descr"><span><?php echo $descr ?></span></p>
                        <?php } ?>
                      </a>
                    </li>
                <?php endforeach; ?>
            </ul>
        <?php endif; ?>
  </div>
</section>