<section class="teams-single">
  <div class="container">
    <div class="teams-single__wrapper">
      <div class="teams-single__info">
        <img  class="teams-single__img" src="http://dev.pacnew/wp-content/uploads/2021/02/pexels-arturas-kokorevas-5168539-scaled.jpg" alt="Zundra Bateaste-Sutton">
        <div class="teams-single__info-content">
          <span class="teams-single__position">AMBASSADOR</span>
          <h1 class="teams-single__title">Zundra Bateaste-Sutton </h1>
          <span class="teams-single__text">Mississippi U=U Ambassador</span>
        </div>
      </div>
      <div class="teams-single__description">
        <strong class="teams-single__subtitle">“Try to give more to this world than you take from it!”</strong>
        <p class="teams-single__text">
        Zundra Bateaste-Sutton is a Mississippi-born Forensic Epidemiologist & Biostatistician. She is the Co-founder and Executive Director of CryOut Teen Organization, a nonprofit designed with the mission of “changing youths’ outlook on life.” 

As the fifth child of eight and a “GRITS” -a Girl Raised In The South- Zundra realized that life would never be a cakewalk even with hardworking parents. So she would not abandon her principles and NEVER back down. Those values pressed her to fight the AIDS epidemic. 

Zundra is a certified HIV/AIDS Train the Trainer, Labels Table Facilitator, Certified Health Education Specialist, PAM-IT Program Coordinator, Specialized Peer Counselor, Lead Youth Educator, Stigmas Stink Diplomat, and McRae Girl’s Home Two of Us Program Director. She distributed over 60,000 free contraceptives through the HBCU Tour (paired with national Sororities/Fraternities) that she and her son started in 2016. Zundra has been a public speaker for over 16 years. She has also written 14 grants and 67 mini-grants, focusing on access to healthcare, inequities in education, and stamping out stigmas.
 
Zundra was employed by the Mississippi State Department of Health. She was the State Evaluator and worked closely with the MSDH STD/HIV Office, American Civil Liberties Union, ManUp Mississippi, M2M Mentor Program, and the Mississippi Rural Health Association. 
 
Zundra received the Mississippi Distinction Fundraising Award 2008-2010. She is a member of the National Health Honorary Eta Sigma Gamma, Alpha Iota Chapter (President 2002-2004). Zundra was featured in the Jacksonian after publishing the first of her three novels, Nothing Short of Amazing: Truly Awesome. She received her undergraduate degree from the University of Southern Mississippi and her graduate and post-graduate degrees from Jackson State University and The University of Mississippi Medical Center.  
 
Outside of her diligent advocacy efforts, Zundra is a devoted wife to her loving husband, Dallas, and an active mother to her daughter, Dallas Rai, and son, Jaidon. She is the youth director at her local church and enjoys singing and playing the piano. 
        </p>
      </div>
    </div>
  </div>
</section>