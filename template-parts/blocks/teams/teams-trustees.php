<section class="section-teams">
  <div class="container">
    <div class="section-teams__top">
      <h2 class="section-teams__title"><?php echo get_field('teams_trustees_title') ?></h2>
    </div>
    <?php
        $article_posts = get_field('teams_trustees_item');
        if( $article_posts ): ?>
            <ul class="teams-trustees">
                <?php foreach( $article_posts as $article_post ): 
                    $permalink = get_permalink( $article_post->ID );
                    $descr = get_the_excerpt( $article_post->ID );
                    $title = get_the_title( $article_post->ID );
                    $img = get_the_post_thumbnail_url( $article_post->ID );
                    $termsLocation = get_the_terms($article_post->ID, 'location');
                    ?>
                    <li class="teams-trustees__item">
                      <a href="<?php echo $permalink ?>" class="teams-trustees__img">
                        <img src="<?php echo $img ?>" alt="<?php echo $title ?>">
                      </a>
                      <h3 class="teams-trustees__title">
                        <a href="<?php echo $permalink ?>"><?php echo $title ?></a>
                      </h3>
                      <div class="teams-trustees__tag">
                        <?php echo pac_svg('location'); ?>
                        <?php
                            if( is_array( $termsLocation ) ){
                              foreach( $termsLocation as $cur_loc ){
                                echo '<span>'. $cur_loc->name .'</span>';
                              }
                            }
                         ?>
                      </div>
                    </li>
                <?php endforeach; ?>
            </ul>
        <?php endif; ?>
  </div>
</section>