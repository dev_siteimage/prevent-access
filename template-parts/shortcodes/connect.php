<?php 
  add_shortcode('connect', function() {
    
     $atts = shortcode_atts( array(
         'start' => '',
         'end' => '',
     ), $atts );

    ob_start(); ?>
<ul class="connect">

    <li class="connect__item"><?php echo get_field('connect_name', 'option'); ?></li>
    <li class="connect__item"><?php echo get_field('connect_number', 'option'); ?></li>
    <li class="connect__item">
      <span><?php echo get_field('connect_state', 'option'); ?> | </span>
      <span><?php echo get_field('connect_zip', 'option'); ?></span>
    </li>
    <li class="connect__item"><?php echo get_field('connect_country', 'option'); ?></li>
    <li class="connect__item"><a href="tel:<?php echo get_field('connect_phone', 'option'); ?>">Tel:<?php echo get_field('connect_phone', 'option'); ?></a></li>

</ul>

<?php return ob_get_clean();
  });
