<?php 
  add_shortcode('instagram_posts', function() {
    
     $atts = shortcode_atts( array(
         'start' => '',
         'end' => '',
     ), $atts );

    ob_start(); ?>
    <section class=" section section-instagram-post">
      <div class="container">

        <h2 class="section-title section-title--instagram"><span><?php _e('Our ') ?></span><?php _e('instagram') ?></h2>

      <ul class="instagram-post">
        <?php $the_query = new WP_Query( 'posts_per_page=6' ); ?>
        <?php while ($the_query -> have_posts()) : $the_query -> the_post(); ?>
        <li class="instagram-post__item">
          <a class="instagram-post__link" href="<?php the_permalink() ?>">
            <?php the_post_thumbnail(); ?>
            <h3 class="instagram-post__title"><?php the_title(); ?></h3>
          </a>
        </li>
        <?php endwhile; wp_reset_postdata(); ?>
      </ul>
      </div>
    </section>

<?php return ob_get_clean();
  });
