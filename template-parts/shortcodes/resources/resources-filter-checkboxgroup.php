<?php

/**
 * @var array $args
 */

$name = array_key_exists('name', $args) ? $args['name'] : '';
$svg = array_key_exists('svg', $args) ? $args['svg'] : '';
$label = array_key_exists('label', $args) ? $args['label'] : '';
$options = array_key_exists('options', $args) ? $args['options'] : [];
$value = array_key_exists('value', $args) ? $args['value'] : null;
$action = array_key_exists('action', $args) ? $args['action'] : '';

?>
<div class="filter" data-filter>
    <strong class="filter__title">
        <?php
        echo $svg;
        echo $label;
        ?>
    </strong>
    <div class="filter__content" data-filter-content>
        <?php
        $i = 0;
        foreach ($options as $option => $optionLabel) : ?>
        <label class="s-input" data-filter-url>
            <?php
            if (is_array($value)) {
                $checked = in_array($option, $value);
            } else {
                $checked = ($value == $option);
            }
            ?>
            <input type="checkbox"
                   name="<?php echo $name . '[' . $i++ . ']'; ?>"
                   id=""
                   class="hidden"
                   value="<?php echo $option; ?>"
                <?php echo $checked ? 'checked="checked" ' : ''; ?>
                   data-url="<?php echo $checked ? $action : ($action . '?' . $name . '=' . $option); ?>"
            >
            <span class="s-input__icon"></span>
            <span class="s-input__text"><?php echo $optionLabel; ?></span>
        </label>
        <?php endforeach; ?>
    </div>
    <?php if (count($options) > 4) : ?>
    <button class="filter__more" type="button" data-filter-more>
        <span><?php _e('View more', 'pac') ?></span><?php echo pac_svg('chevron'); ?></button>
    <?php endif; ?>
</div>
