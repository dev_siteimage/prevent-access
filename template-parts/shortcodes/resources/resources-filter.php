<?php

/**
 * @var array $args
 */

$filters = array_key_exists('filters', $args) ? $args['filters'] : [];
global $wp;
$action = home_url( $wp->request );
$keywords = array_key_exists('keywords', $filters) ? $filters['keywords']['value'] : '';
$startDate = array_key_exists('start_date', $filters) ? $filters['start_date']['value'] : '';
$endDate = array_key_exists('end_date', $filters) ? $filters['end_date']['value'] : '';

?>
<form method="get" action="<?php echo $action; ?>">
    <div class="filter">
        <strong class="filter__title filter__title--no-border">
            <?php echo pac_svg('search'); ?>
            <?php _e('Keywords') ?>
        </strong>
        <div class="filter__wrapper">
            <input class="filter__input" type="text" name="keywords" id="keywords" placeholder="Search by Keywords" value="<?php echo $keywords; ?>">
        </div>
        <strong class="filter__title filter__title--no-border">
            <?php echo pac_svg('calendar'); ?>
            <?php _e('Date') ?>
        </strong>
        <div class="filter__wrapper">
            <input class="filter__input" type="date" name="start_date" id="start_date"
                   placeholder="Start date" value="<?php echo $startDate; ?>">
            <input class="filter__input" type="date" name="end_date" id="end_date"
                   placeholder="End date" value="<?php echo $endDate; ?>">
            <button class="btn" type="submit"><?php _e('Search', 'pac') ?></button>
        </div>
    </div>
</form>
<form method="get" action="<?php echo $action; ?>" data-filter-form>
<?php
// Countries filter
if (array_key_exists('country', $filters)) {
    $filter = $filters['country'];
    $filter['svg'] = pac_svg('location');
    $filter['action'] = $action;

    get_template_part('template-parts/shortcodes/resources/resources-filter', 'checkboxgroup', $filter);
}

// Resource types filter
if (array_key_exists('resource-type', $filters)) {
    $filter = $filters['resource-type'];
    $filter['svg'] = pac_svg('filter');
    $filter['action'] = $action;

    get_template_part('template-parts/shortcodes/resources/resources-filter', 'checkboxgroup', $filter);
}

?>
</form>
