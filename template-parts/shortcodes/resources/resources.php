<?php

/**
 * @var array $args
 * @var WP_Query $resourcesQuery
 * @var array $filters
 * @var int $page
 */

$resourcesQuery = array_key_exists('resourcesQuery', $args) ? $args['resourcesQuery'] : null;
$filters = array_key_exists('filters', $args) ? $args['filters'] : null;
$page = array_key_exists('page', $args) ? $args['page'] : null;
$activeFilters = array_key_exists('activeFilters', $args) ? $args['activeFilters'] : [];

global $wp;
$action = home_url( $wp->request );

?>
<section class="section section-resources" data-filter-section>
    <div class="container">

        <div class="section-resources__filter-button">
            <button class="btn btn--large btn--icon"
                    data-filter-mobile-button><?php echo pac_svg('filter'); ?><?php _e('Filter', 'pac') ?></button>
        </div>
        <?php if (count($activeFilters)) : ?>
            <div class="section-resources__clear">
                <a class="btn" href="<?php echo $action; ?>"><?php _e('Clear all filters');?></a>
            </div>
        <?php endif; ?>
        <div class="section-resources__wrapper">

            <aside class="section-resources__sidebar" data-filter-mobile>
                <?php

                get_template_part('template-parts/shortcodes/resources/resources-filter', null, compact('filters'));

                ?>
            </aside>


            <div class="section-resources__content">
                <?php
                if ($resourcesQuery->have_posts()) : ?>
                <ul class="resources-list">
                        <?php while ($resourcesQuery->have_posts()) : $resourcesQuery->the_post(); ?>
                            <li class="resources-list__item">
                                <span class="resources-list__text"><?php echo get_the_date( 'F j, Y' ); ?></span>
                                <h3 class="resources-list__title">
                                    <a
                                        href="<?php if(get_field( 'resources_post_file' )) {
                                            echo get_field( 'resources_post_file' );
                                        } elseif   (get_field( 'resources_post_link' )) {
                                            echo get_field( 'resources_post_link' );
                                        } else {
                                            the_permalink();
                                        }
                                          ?>  "target="_blank"><?php the_title(); ?></a></h3>
                                <?php
                                $countries = get_the_terms(get_the_ID(), 'country');
                                if ($countries) { ?>
                                    <div class="resources-list__text">
                                        <div class="resources-list__icon"><?php echo pac_svg('location'); ?></div>
                                        <?php foreach ($countries as $country) {
                                            echo '<a href="' . get_category_link($country->term_id) . '">' . $country->name . '</a>';
                                        } ?>
                                    </div>
                                <?php } ?>
                                <?php
                                $keywords = get_the_terms(get_the_ID(), 'resource-type');
                                if ($keywords) { ?>
                                    <div class="resources-list__text">
                                        <div class="resources-list__icon"><?php echo pac_svg('filter'); ?></div>
                                        <?php
                                        foreach ($keywords as $tag) {
                                            echo '<a href="' . get_tag_link($tag->term_id) . '">' . $tag->name . '</a>';
                                        } ?>
                                    </div>
                                <?php } ?>
                            </li>

                        <?php endwhile; ?>


                </ul>
                    <div class="pagination">
                        <?php
                        $big = 999999999; // need an unlikely integer
                        echo paginate_links( array(
                            'base' => str_replace( $big, '%#%', esc_url( get_pagenum_link( $big ) ) ),
                            'format' => '?paged=%#%',
                            'current' => max( 1, get_query_var('paged') ),
                            'total' => $resourcesQuery->max_num_pages,
                            'prev_text' => '<span>< Prev</span>',
                            'next_text' => '<span>Next ></span>'
                        ) );
                        ?>

                    </div>
                <?php else: ?>
                <p class="section-resources__empty-title">
                    <?php _e('Sorry, no results were found.'); ?>
                </p>
                <p class="section-resources__empty-subtitle">
                    <?php _e('Search Suggestions'); ?>
                </p>
                <ul class="section-resources__empty-subtext">
                    <li><?php _e('Try to insert other dates'); ?></li>
                    <li><?php _e('Check your spelling'); ?></li>
                    <li><?php _e('Try to choose different Countries or Resource Types.'); ?></li>
                </ul>
                <?php endif; ?>
                <?php
                wp_reset_postdata();
                ?>
            </div>
        </div>
    </div>
</section>
