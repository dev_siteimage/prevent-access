<?php 
  add_shortcode('latest_news', function() {
    
     $atts = shortcode_atts( array(
         'start' => '',
         'end' => '',
     ), $atts );

    ob_start(); ?>
    <section class=" section section-latest-news">
      <div class="container">

      <div class="section-latest-news__top">
        <h2 class="section-title"><?php echo get_field('lates_news_title', 'option'); ?></h2>
        <div class="section-latest-news__content">
          <p class="section-latest-news__text"><?php echo get_field('lates_news_text', 'option'); ?></p>
          <a href="<?php echo get_permalink( get_page_by_path( 'donation' ) ); ?>" class="btn btn--large"><?php echo get_field('lates_news_button_text', 'option'); ?></a>
        </div>
      </div>

      <ul class="latest-news">
        <?php $the_query = new WP_Query( 'posts_per_page=3' ); ?>
        <?php while ($the_query -> have_posts()) : $the_query -> the_post(); ?>
        <li class="latest-news__item">
          <a class="latest-news__link" href="<?php the_permalink() ?>">
            <?php the_post_thumbnail(); ?>
            <h3 class="latest-news__title"><?php the_title(); ?></h3>
          </a>
        </li>
        <?php endwhile; wp_reset_postdata(); ?>
      </ul>
      </div>
    </section>

<?php return ob_get_clean();
  });
