<?php

if (isset($args)) {
    extract($args);
}
/**
 * @var string $prepend
 * @var bool $showname
 * @var string $nametxt
 * @var string $firstnametxt
 * @var string $lastnametxt
 * @var string $nameholder
 * @var string $firstnameholder
 * @var string $lastnameholder
 * @var string $emailtxt
 * @var string $emailholder
 * @var bool $showsubmit
 * @var string $submittxt
 * @var bool $jsthanks
 * @var string $thankyou
 */

?>
<form class="sml_subscribe" method="post"><input class="sml_hiddenfield" name="sml_subscribe" type="hidden" value="1">
    <?php if (!empty($prepend)) { ?>
        <p class="prepend"><?php echo $prepend; ?></p>
    <?php } ?>
    <?php if (array_key_exists('sml_subscribe', $_POST) && $_POST['sml_subscribe'] && $thankyou) {
        if ($jsthanks) {?>
            <script>window.onload = function() { alert('<?php echo $thankyou; ?>'); }</script>
        <?php } else { ?>
            <p class="sml_thankyou">'<?php echo $thankyou; ?>'</p>
    <?php }} ?>
    <?php if ($showname) { ?>
        <p class="sml_name sml_firstname">
            <label class="sml_firstnamelabel" for="sml_firstname"><?php echo $firstnametxt?></label>
            <input class="sml_firstnameinput" placeholder="<?php echo $firstnameholder?>" name="sml_firstname" id="sml_firstname" type="text" value="">
        </p>
        <p class="sml_name sml_lastname">
            <label class="sml_lastnamelabel" for="sml_lastname"><?php echo $lastnametxt?></label>
            <input class="sml_lastnameinput" placeholder="<?php echo $lastnameholder?>" name="sml_lastname" id="sml_lastname" type="text" value="">
        </p>
        <input class="sml_nameinput" placeholder="<?php echo $lastnameholder?>" name="sml_name" id="sml_name" type="hidden" value="">
        <script>
            window.onload = function () {
                jQuery(function ($) {
                    let firstname = $('.sml_firstname > input[name=sml_firstname]');
                    let lastname = $('.sml_lastname > input[name=sml_lastname]');
                    let name = $('input[name=sml_name]');
                    firstname.on('change', function () {
                        name.val($(this).val() + ' ' + lastname.val())
                    });
                    lastname.on('change', function () {
                        name.val(firstname.val() + ' ' + $(this).val())
                    });
                });
            }
        </script>
    <?php } ?>
    <p class="sml_email">
        <label class="sml_emaillabel" for="sml_email"><?php echo $emailtxt; ?></label>
        <input class="sml_emailinput" name="sml_email" placeholder="<?php echo $emailholder; ?>" type="text" value="">
    </p>
    <?php if ($showsubmit) { ?>
        <p class="sml_submit">
            <input name="submit" class="btn sml_submitbtn" type="submit" value="<?php echo $submittxt; ?>">
        </p>
    <?php } ?>
</form>


<form class="sml_subscribe" method="post">
    <input class="sml_hiddenfield" name="sml_subscribe" type="hidden" value="1">
    <p class="prepend">Sign-up for email updates</p>
    <p class="sml_name">
        <label class="sml_namelabel" for="sml_name"></label>
        <input class="sml_nameinput"
               placeholder="First Name"
               name="sml_name" type="text" value="">
    </p>
    <p class="sml_email">
        <label class="sml_emaillabel" for="sml_email"></label>
        <input class="sml_emailinput"
               name="sml_email"
               placeholder="E-mail" type="text"
               value="">
    </p>
    <p class="sml_submit"><input name="submit" class="btn sml_submitbtn" type="submit" value="Sign Up"></p></form>


<form class="sml_subscribe" method="post">
    <input class="sml_hiddenfield" name="sml_subscribe" type="hidden" value="1">
    <p class="prepend">Sign-up for email updates</p>
    <p class="sml_name sml_firstname">
        <label class="sml_firstnamelabel" for="sml_firstname">First Name:</label>
        <input class="sml_firstnameinput" placeholder="First Name..." name="sml_firstname" id="sml_firstname" type="text" value="">
    </p>
    <p class="sml_name sml_lastname">
        <label class="sml_lastnamelabel" for="sml_lastname">Last Name:</label>
        <input class="sml_lastnameinput" placeholder="Last Name..." name="sml_lastname" id="sml_lastname" type="text" value="">
    </p>
    <input class="sml_nameinput" placeholder="Last Name..." name="sml_name" id="sml_name" type="hidden" value="">
    <script>
        window.onload = function () {
            jQuery(function ($) {
                let firstname = $('.sml_firstname > input[name=sml_firstname]');
                let lastname = $('.sml_lastname > input[name=sml_lastname]');
                let name = $('input[name=sml_name]');
                firstname.on('change', function () {
                    name.val($(this).val() + ' ' + lastname.val())
                });
                lastname.on('change', function () {
                    name.val(firstname.val() + ' ' + $(this).val())
                });
            });
        }
    </script>
    <p class="sml_email">
        <label class="sml_emaillabel" for="sml_email"></label>
        <input class="sml_emailinput" name="sml_email" placeholder="E-mail" type="text" value="">
    </p>
    <p class="sml_submit">
        <input name="submit" class="btn sml_submitbtn" type="submit" value="Sign Up">
    </p>
</form>
