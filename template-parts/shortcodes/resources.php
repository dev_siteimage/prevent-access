<?php

/**
 * @var array $args
 * @var WP_Query $resourcesQuery
 * @var array $filters
 * @var int $page
 */

$resourcesQuery = array_key_exists('resourcesQuery', $args) ? $args['resourcesQuery'] : null;
$filters = array_key_exists('filters', $args) ? $args['filters'] : null;
$page = array_key_exists('page', $args) ? $args['page'] : null;

var_dump($resourcesQuery);
var_dump($filters);

?>
<section class="section section-resources" data-filter-section>
    <div class="container">

        <div class="section-resources__filter-button">
            <button class="btn btn--large btn--icon"
                    data-filter-mobile-button><?php echo pac_svg('filter'); ?><?php _e('Filter', 'pac') ?></button>
        </div>
        <div class="section-resources__wrapper">

            <aside class="section-resources__sidebar" data-filter-mobile>

                <div class="filter">
                    <strong class="filter__title filter__title--no-border">
                        <?php echo pac_svg('search'); ?>
                        <?php _e('Keywords') ?>
                    </strong>
                    <div class="filter__wrapper">
                        <input class="filter__input" type="keywords" name="" id="" placeholder="Search by Keywords">
                    </div>
                    <strong class="filter__title filter__title--no-border">
                        <?php echo pac_svg('calendar'); ?>
                        <?php _e('Date') ?>
                    </strong>
                    <div class="filter__wrapper">
                        <input class="filter__input" placeholder="Date" type="date" name="start_date" id="start_date"
                               placeholder="Start date">
                        <input class="filter__input" placeholder="Date" type="date" name="end_date" id="end_date"
                               placeholder="End date">
                        <button class="btn" type="submit"><?php _e('Search', 'pac') ?></button>
                    </div>
                </div>
                <div class="filter" data-filter>
                    <strong class="filter__title">
                        <?php echo pac_svg('location'); ?>
                        <?php _e('Country') ?>
                    </strong>
                    <div class="filter__content" data-filter-content>
                        <?php
                        $categories = $categories = get_categories([
                            'taxonomy' => 'category',
                            'type' => 'post',
                            'child_of' => 0,
                            'parent' => '',
                            'orderby' => 'name',
                            'order' => 'ASC',
                            'hide_empty' => 1,
                            'hierarchical' => 1,
                            'exclude' => '',
                            'include' => '',
                            'number' => 0,
                            'pad_counts' => false,
                        ]);

                        if ($categories) {
                            foreach ($categories as $cat) { ?>
                                <label class="s-input">
                                    <input type="checkbox" name="" id="" class="hidden">
                                    <span class="s-input__icon"></span>
                                    <span class="s-input__text"><?php echo $cat->name; ?></span>
                                </label>
                            <? }
                        }
                        ?>
                    </div>
                    <button class="filter__more" type="button" data-filter-more>
                        <span><?php _e('View more', 'pac') ?></span><?php echo pac_svg('chevron'); ?></button>
                </div>
                <div class="filter" data-filter>
                    <strong class="filter__title">
                        <?php echo pac_svg('filter'); ?>
                        <?php _e('Resource Type') ?>
                    </strong>
                    <div class="filter__content" data-filter-content>
                        <?php
                        $tags = $tags = get_tags([
                            'taxonomy' => 'post_tag',
                            'orderby' => 'name',
                            'hide_empty' => false // for development
                        ]);

                        if ($tags) {
                            foreach ($tags as $tag) { ?>
                                <label class="s-input">
                                    <input type="checkbox" name="" id="" class="hidden">
                                    <span class="s-input__icon"></span>
                                    <span class="s-input__text"><?php echo $tag->name; ?></span>
                                </label>
                            <? }
                        }
                        ?>
                    </div>
                    <button class="filter__more" type="button" data-filter-more>
                        <span><?php _e('View more', 'pac') ?></span><?php echo pac_svg('chevron'); ?></button>
                    <div>
            </aside>


            <div class="section-resources__content">
                <ul class="resources-list">
                    <?php
                    if ($resourcesQuery->have_posts()) : ?>
                        <?php while ($resourcesQuery->have_posts()) : $resourcesQuery->the_post(); ?>
                            <li class="resources-list__item">
                                <span class="resources-list__text"><?php the_date(); ?></span>
                                <h3 class="resources-list__title"><a
                                        href="<?php the_permalink(); ?>  "><?php the_title(); ?></a></h3>
                                <?php if (get_the_category()) { ?>
                                    <div class="resources-list__text">
                                        <div class="resources-list__icon"><?php echo pac_svg('location'); ?></div>
                                        <?php foreach (get_the_category() as $category) {
                                            echo '<a href="' . get_category_link($category->term_id) . '">' . $category->name . '</a>';
                                        } ?>
                                    </div>
                                <?php } ?>
                                <?php if (get_the_tags()) { ?>
                                    <div class="resources-list__text">
                                        <div class="resources-list__icon"><?php echo pac_svg('filter'); ?></div>
                                        <?php
                                        foreach (get_the_tags() as $tag) {
                                            echo '<a href="' . get_tag_link($tag->term_id) . '">' . $tag->name . '</a>';
                                        } ?>
                                    </div>
                                <?php } ?>
                            </li>

                        <?php endwhile; ?>
                    <?php endif;
                    wp_reset_postdata(); ?>

                </ul>
            </div>
        </div>
    </div>
</section>

