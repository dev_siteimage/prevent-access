<?php

  add_shortcode('privacy_menu', function ($atts){

    $atts = shortcode_atts( array(
        'title'      => '',
        'template'   => 'privacy-menu',

    ), $atts, 'privacy_menu' );

    ob_start();

        wp_nav_menu( [
        'theme_location'  => 'privacy',
        'container'       => 'div', 
        'container_class' => '', 
        'container_id'    => '',
        'menu_class'      => 'menu privacy-menu',
        'echo'            => true,
        'items_wrap'      => '<ul id="%1$s" class="%2$s">%3$s</ul>',
        ] );

    return ob_get_clean();

});