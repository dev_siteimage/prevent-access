<header class="header">
  <div class="header__top">
      <p class="header__top-text"><?php echo get_field('header_top_text', 'option') ?></p>
      <a href="<?php echo get_field('header_top_link', 'option')?>" class="header__top-link"><?php echo get_field('header_top_link_text', 'option')?></a>
  </div>
  <div class="header__content">
      <a class="logo" href="{{ home_url('/') }}">
        <img class="logo__img" src="<?php bloginfo('template_directory'); ?>/resources/assets/images/logo.png" alt="Logo Customer">
      </a>
    
      <nav class="nav-primary">
        @if (has_nav_menu('primary_navigation'))
          {!! wp_nav_menu(['theme_location' => 'primary_navigation', 'menu_class' => 'main-menu js-main-menu', 'echo' => false]) !!}
        @endif
      </nav>
    
      <a href="<?php echo get_field('header_button_link', 'option') ?>" target="_blank" class="btn"><?php echo get_field('header_button_text', 'option') ?></a>

      <div class="hamburger" data-menu-button>
        <span></span>
      </div>

  </div>
</header>
