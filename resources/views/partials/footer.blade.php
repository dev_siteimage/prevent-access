<footer class="footer">
  <div class="footer__donate">
    <span class="footer__donate-text"><?php echo get_field('footer_text_message', 'option')?></span>
    <a href="<?php echo get_permalink( get_page_by_path( 'donation' ) ); ?>" class="btn btn--blue btn--large"><?php _e('Donate now') ?></a>
  </div>
  <div class="container">
    <div class="footer__content">
      @php(dynamic_sidebar('sidebar-footer'))
    </div>
    <div class="footer__bottom">
      <p class="footer__bottom-text"><?php echo get_field('footer_text_one', 'option')?></p>
      <p class="footer__bottom-copy"><?php echo get_field('footer_text_two', 'option')?></p>
    </div>
  </div>
</footer>
