{{-- <article @php(post_class())>
  <header>
    <h1 class="entry-title">
      {!! $title !!}
    </h1>

    @include('partials/entry-meta')
  </header>

  <div class="entry-content">
    @php(the_content())
  </div>

  <footer>
    {!! wp_link_pages(['echo' => 0, 'before' => '<nav class="page-nav"><p>' . __('Pages:', 'sage'), 'after' => '</p></nav>']) !!}
  </footer>

  @php(comments_template())
</article> --}}


<section class="teams-single">
  <div class="container">
    <div class="teams-single__wrapper">
      <div class="teams-single__info">
        <div class="teams-single__img">
          <?php the_post_thumbnail(); ?>
        </div>
        <div class="teams-single__info-content">
          <?php
            $roles = get_the_terms(get_the_ID(), 'role');
            $locations = get_the_terms(get_the_ID(), 'location');
            $keywords = get_the_terms(get_the_ID(), 'resource-type');
            $countries = get_the_terms(get_the_ID(), 'country');
            if ($roles) { 
                foreach ($roles as $role) {
                    echo '<span class="test teams-single__position">' . $role->name . '</span>';
                } 
            } ?>
          <h1 class="teams-single__title">{!! $title !!}</h1>
          <span class="teams-single__text">
            <?php
              if ($locations) { 
                foreach ($locations as $location) {
                    echo '<span>' . $location->name . '</span><span> U=U </span> ';
                } 
            } ?>
            <?php 
            if ($roles) { 
                foreach ($roles as $role) {
                    echo '<span>' . $role->name . '</span>';
                } 
            } ?>
          </span>
          <?php 
            if ($keywords) { ?>
                <span class="teams-single__text teams-single__text--block">
                  <span><?php echo pac_svg('filter'); ?></span>
                  <?php foreach ($keywords as $keyword) {
                      echo '<span>' . $keyword->name . '</span> ';
                  } ?>
                </span>
            <?php } ?>
            <?php 
            if ($countries) {  ?>
                <span class="teams-single__text teams-single__text--block">
                  <span><?php echo pac_svg('location'); ?></span>
                  <?php foreach ($countries as $country) {
                      echo '<span>' . $country->name . '</span> ';
                  } ?>
              </span>
            <?php } ?>
          <?php $postSocial = get_field('social_post_item'); 
            if ($postSocial) { ?>
              <ul class="social">
                <?php foreach($postSocial as $postSocialItem) : 
                    if ($postSocialItem['social_post_item_icon']) : ?>
                      <li class="social__item">
                          <a class="social__link" href="<?php echo $postSocialItem['social_post_item_url']; ?>">
                              <span class="social__icon"><?php echo file_get_contents($postSocialItem['social_post_item_icon']); ?></span>
                          </a>
                      </li>
                <?php endif; endforeach; ?>
              </ul>

            <?php } ?>
          
        </div>
      </div>
      <div class="teams-single__description">
        <?php
          $phrase = get_field('teams_post_phrase');
          if($phrase) { ?>
            <strong class="teams-single__subtitle"><?php echo $phrase; ?></strong>
         <?php } ?>
        <?php the_content(); ?>
      </div>
    </div>
  </div>
</section>