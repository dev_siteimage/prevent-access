@extends('layouts.app')

@section('content')
  <div class="not-found">
    <div class="container">
      <span class="not-found__number">
        <span class="not-found__item">{!! __('4', 'sage') !!}</span>
        <span class="not-found__item">{!! __('0', 'sage') !!}</span>
        <span class="not-found__item">{!! __('4', 'sage') !!}</span>
      </span>
      @include('partials.page-header')
  
      @if (! have_posts())
        <x-alert type="warning">
          {!! __('Sorry, but the page you are trying to view does not exist.', 'sage') !!}
        </x-alert>
  
      @endif
  
    </div>

  </div>
@endsection
