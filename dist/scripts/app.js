(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["/scripts/app"],{

/***/ "./resources/assets/scripts/app.js":
/*!*****************************************!*\
  !*** ./resources/assets/scripts/app.js ***!
  \*****************************************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* WEBPACK VAR INJECTION */(function($) {/* harmony import */ var jquery__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! jquery */ "jquery");
/* harmony import */ var jquery__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(jquery__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var slick_carousel__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! slick-carousel */ "./node_modules/slick-carousel/slick/slick.js");
/* harmony import */ var slick_carousel__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(slick_carousel__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var country_select_js_build_js_countrySelect__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! country-select-js/build/js/countrySelect */ "./node_modules/country-select-js/build/js/countrySelect.js");
/* harmony import */ var country_select_js_build_js_countrySelect__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(country_select_js_build_js_countrySelect__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var jquery_validation_dist_jquery_validate__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! jquery-validation/dist/jquery.validate */ "./node_modules/jquery-validation/dist/jquery.validate.js");
/* harmony import */ var jquery_validation_dist_jquery_validate__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(jquery_validation_dist_jquery_validate__WEBPACK_IMPORTED_MODULE_3__);
/**
 * External Dependencies
 */




$(document).ready(function () {
  $(document).on('click', '[data-menu-button]', function () {
    var th = $(this);
    var menu = $('.nav-primary');
    $('body').toggleClass('fixed');
    th.toggleClass('active');
    menu.toggleClass('active');
  });
  $(window).on('load', function () {
    if ($(".js-main-menu li").find(".sub-menu").length > 0) {
      $(this).addClass('active');
    }
  });
  $(document).on('click', '[data-video-button]', function () {
    var th = $(this);
    var videoContainer = th.closest('[data-video-container]');
    var video = videoContainer.find('[data-video]');

    if (th.hasClass('play')) {
      th.removeClass('play');
      th.addClass('paused');
      video.trigger('pause');
    } else if (th.hasClass('paused')) {
      th.addClass('play');
      th.removeClass('paused');
      video.trigger('play');
    }
  });
  $(window).scroll(function () {
    var scroll = $(window).scrollTop();
    var video = $("#site-video");

    if (scroll >= 1) {
      video.addClass("fixed");
    } else {
      video.removeClass("fixed");
    }
  });
  $(document).on('click', '[data-filter-more]', function () {
    var th = $(this);
    var container = th.closest('[data-filter]');
    var target = container.find('[data-filter-content]');
    var scrollHeight = target.prop('scrollHeight');

    if (th.hasClass('active')) {
      th.removeClass('active');
      target.css('max-height', '170px');
    } else {
      th.addClass('active');
      target.css('max-height', scrollHeight);
    }
  });
  $(document).on('click', '[data-filter-url]', function () {
    $(this).closest('[data-filter-form]').submit();
    localStorage.setItem('refresh', true);
  });
  $(window).on('load', function () {
    if (localStorage.getItem('refresh') == 'true') {
      var headerHeight = $('.header').height();
      $('html, body').animate({
        scrollTop: $(".section-resources").offset().top - headerHeight
      }, 2000);
    }
  });
  $(document).on('click', '.logo', function () {
    localStorage.removeItem('refresh');
  });
  $(document).on('click', '[data-filter-mobile-button]', function () {
    var th = $(this);
    var container = th.closest('[data-filter-section]');
    var target = container.find('[data-filter-mobile]');

    if (th.hasClass('active')) {
      th.removeClass('active');
      target.removeClass('active');
    } else {
      th.addClass('active');
      target.addClass('active');
    }
  });
  $(document).on('click', '[data-accordion-button]', function () {
    var th = $(this);
    var container = th.closest('[data-accordion]');
    var target = container.find('[data-accordion-content]');

    if (th.hasClass('active')) {
      th.removeClass('active');
      target.slideUp();
    } else {
      th.addClass('active');
      target.slideDown();
    }
  });
  $('[data-articles-slider]').slick({
    infinite: true,
    slidesToShow: 3,
    slidesToScroll: 1,
    prevArrow: $('[data-articles-slider-prev]'),
    nextArrow: $('[data-articles-slider-next]'),
    responsive: [{
      breakpoint: 1024,
      settings: {
        slidesToShow: 2
      }
    }, {
      breakpoint: 576,
      settings: {
        slidesToShow: 1,
        slidesToScroll: 1
      }
    }]
  });
  $(document).on('click', '[data-scroll-form]', function () {
    var headerHeight = $('.header').height();
    $('html, body').animate({
      scrollTop: $("#aplication-form").offset().top - headerHeight
    }, 2000);
  });
  var controller = new ScrollMagic.Controller();
  $('.timeline__item:nth-child(odd) [data-timeline]').each(function () {
    var tween = new TimelineMax();
    var fromLeftFrom = TweenMax.from($(this), 0.5, {
      x: 200,
      opacity: 0,
      delay: 0.5,
      ease: Linear.easeNone
    });
    var fromLeftTo = TweenMax.to($(this), 0.5, {
      x: 0,
      opacity: 1,
      delay: 0.5,
      ease: Linear.easeNone
    });
    tween.add(fromLeftFrom).add(fromLeftTo);
    var scene = new ScrollMagic.Scene({
      triggerElement: this,
      offset: '50%'
    }).setTween(tween).addTo(controller);
  });
  $('.timeline__item:nth-child(even) [data-timeline]').each(function () {
    var tween = new TimelineMax();
    var fromLeftFrom = TweenMax.from($(this), 0.5, {
      x: -200,
      opacity: 0,
      delay: 0.5,
      ease: Linear.easeNone
    });
    var fromLeftTo = TweenMax.to($(this), 0.5, {
      x: 0,
      opacity: 1,
      delay: 0.5,
      ease: Linear.easeNone
    });
    tween.add(fromLeftFrom).add(fromLeftTo);
    var scene = new ScrollMagic.Scene({
      triggerElement: this,
      offset: '50%'
    }).setTween(tween).addTo(controller);
  });
  $('.timeline__center').each(function () {
    var tween = new TimelineMax();
    var fromLeftFrom = TweenMax.from($(this), 0.5, {
      scaleY: 0,
      opacity: 0,
      transformOrigin: "bottom",
      ease: Linear.easeNone
    });
    var fromLeftTo = TweenMax.to($(this), 0.5, {
      scaleY: 1,
      opacity: 1,
      transformOrigin: "bottom",
      ease: Linear.easeNone
    });
    tween.add(fromLeftFrom).add(fromLeftTo);
    var scene = new ScrollMagic.Scene({
      triggerElement: this.closest('.timeline__item'),
      offset: '0%'
    }).setTween(tween).addTo(controller);
  });
  $("#country").countrySelect();
  jQuery(function ($) {
    $(document).on('click', '[data-contact-form]', function (e) {
      // e.preventDefault();
      var form = $(this).closest('.aplication-form');
      var dataForm = form.serializeArray();
      var overlay = form.find('[data-form-overlay]');
      $(".aplication-form").validate({
        rules: {
          firstname: "required",
          lastname: "required",
          orgname: "required",
          email: {
            required: true,
            email: true
          },
          phone: "required",
          saddres: "required",
          city: "required",
          state: "required",
          zipcode: "required",
          country: "required",
          website: "required",
          message: "required"
        },
        messages: {
          firstname: "Please enter your First Name",
          lastname: "Please enter your Last Name",
          orgname: "Please enter your Organization Name",
          email: "Please enter a valid Email Address",
          phone: "Please enter a valid Phone",
          saddres: "Please enter your Street Addres",
          city: "Please enter your City",
          state: "Please enter your State",
          zipcode: "Please enter your Zipcode",
          country: "Please enter your Country",
          website: "Please enter your Website",
          message: "Please enter your Message"
        },
        submitHandler: function submitHandler() {
          $.ajax({
            url: '/wp-admin/admin-ajax.php?action=process_community_form',
            type: 'POST',
            data: $.param(dataForm),
            beforeSend: function beforeSend(responce) {
              overlay.addClass('active');
            },
            success: function success(responce) {
              overlay.removeClass('active');
              $('.aplication-form')[0].reset();
            }
          });
        }
      });
    });
  });
});
/* WEBPACK VAR INJECTION */}.call(this, __webpack_require__(/*! jquery */ "jquery")))

/***/ }),

/***/ "./resources/assets/styles/app.scss":
/*!******************************************!*\
  !*** ./resources/assets/styles/app.scss ***!
  \******************************************/
/*! no static exports found */
/***/ (function(module, exports) {

// removed by extract-text-webpack-plugin

/***/ }),

/***/ "./resources/assets/styles/editor.scss":
/*!*********************************************!*\
  !*** ./resources/assets/styles/editor.scss ***!
  \*********************************************/
/*! no static exports found */
/***/ (function(module, exports) {

// removed by extract-text-webpack-plugin

/***/ }),

/***/ 0:
/*!************************************************************************************************************************!*\
  !*** multi ./resources/assets/scripts/app.js ./resources/assets/styles/app.scss ./resources/assets/styles/editor.scss ***!
  \************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

__webpack_require__(/*! D:\Work\ProgramsInstalations\OpenServer\domains\dev.pacnew\wp-content\themes\prevent-access\resources\assets\scripts\app.js */"./resources/assets/scripts/app.js");
__webpack_require__(/*! D:\Work\ProgramsInstalations\OpenServer\domains\dev.pacnew\wp-content\themes\prevent-access\resources\assets\styles\app.scss */"./resources/assets/styles/app.scss");
module.exports = __webpack_require__(/*! D:\Work\ProgramsInstalations\OpenServer\domains\dev.pacnew\wp-content\themes\prevent-access\resources\assets\styles\editor.scss */"./resources/assets/styles/editor.scss");


/***/ }),

/***/ "jquery":
/*!**********************************!*\
  !*** external {"this":"jQuery"} ***!
  \**********************************/
/*! no static exports found */
/***/ (function(module, exports) {

(function() { module.exports = this["jQuery"]; }());

/***/ })

},[[0,"/scripts/manifest","/scripts/vendor"]]]);
//# sourceMappingURL=app.js.map