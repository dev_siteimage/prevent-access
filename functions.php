<?php

/*
|--------------------------------------------------------------------------
| Register The Auto Loader
|--------------------------------------------------------------------------
|
| Composer provides a convenient, automatically generated class loader for
| our theme. We will simply require it into the script here so that we
| don't have to worry about manually loading any of our classes later on.
|
*/

if (! file_exists($composer = __DIR__ . '/vendor/autoload.php')) {
    wp_die(__('Error locating autoloader. Please run <code>composer install</code>.', 'sage'));
}

require $composer;

/*
|--------------------------------------------------------------------------
| Register Sage Theme Files
|--------------------------------------------------------------------------
|
| Out of the box, Sage ships with categorically named theme files
| containing common functionality and setup to be bootstrapped with your
| theme. Simply add (or remove) files from the array below to change what
| is registered alongside Sage.
|
*/

collect(['helpers', 'setup', 'filters', 'admin'])
    ->each(function ($file) {
        $file = "app/{$file}.php";

        if (! locate_template($file, true, true)) {
            wp_die(
                sprintf(__('Error locating <code>%s</code> for inclusion.', 'sage'), $file)
            );
        }
    });

/*
|--------------------------------------------------------------------------
| Enable Sage Theme Support
|--------------------------------------------------------------------------
|
| Once our theme files are registered and available for use, we are almost
| ready to boot our application. But first, we need to signal to Acorn
| that we will need to initialize the necessary service providers built in
| for Sage when booting.
|
*/

add_theme_support('sage');

/*
|--------------------------------------------------------------------------
| Turn On The Lights
|--------------------------------------------------------------------------
|
| We are ready to bootstrap the Acorn framework and get it ready for use.
| Acorn will provide us support for Blade templating as well as the ability
| to utilize the Laravel framework and its beautifully written packages.
|
*/

new Roots\Acorn\Bootloader();

require dirname( __FILE__ ) . '/template-parts/shortcodes/init.php';

function pac_svg($file){
  return file_get_contents(__DIR__.'/resources/assets/icons/'.$file.'.svg');
}

add_action( 'after_setup_theme', function(){
	register_nav_menus( [
		'primary'   => __( 'Primary Menu', 'pac' ),
    'privacy' => __( 'Privacy Menu', 'pac' ),
	] );
} );


if( function_exists('acf_add_options_page') ) {

	acf_add_options_page(array(
		'page_title' 	=> 'Theme General Settings',
		'menu_title'	=> 'Theme Settings',
		'menu_slug' 	=> 'theme-general-settings',
		'capability'	=> 'edit_posts',
		'redirect'		=> false
	));

	acf_add_options_sub_page(array(
		'page_title' 	=> 'Theme Social Settings',
		'menu_title'	=> 'Social',
		'parent_slug'	=> 'theme-general-settings',
	));
	acf_add_options_sub_page(array(
		'page_title' 	=> 'Theme Latest News Settings',
		'menu_title'	=> 'Latest News',
		'parent_slug'	=> 'theme-general-settings',
	));
	acf_add_options_sub_page(array(
		'page_title' 	=> 'Theme Connect Settings',
		'menu_title'	=> 'Connect',
		'parent_slug'	=> 'theme-general-settings',
	));
	acf_add_options_sub_page(array(
		'page_title' 	=> 'Theme Footer Settings',
		'menu_title'	=> 'Footer',
		'parent_slug'	=> 'theme-general-settings',
	));
	acf_add_options_sub_page(array(
		'page_title' 	=> 'Theme Header Settings',
		'menu_title'	=> 'Header Text',
		'parent_slug'	=> 'theme-general-settings',
	));

}

function register_acf_block_types (){
	acf_register_block_type(array(
		'name' => 'banner',
		'title' => __('Banner'),
		'description' => 'Custom banner block',
		'render_template' => 'template-parts/blocks/banner/main-banner.php',
		'icon' => 'editor-paste-text',
		'keywords' => array('banner', 'product')
	));
	acf_register_block_type(array(
		'name' => 'map',
		'title' => __('Map'),
		'description' => 'Custom map block',
		'render_template' => 'template-parts/blocks/map/map.php',
		'icon' => 'editor-paste-text',
		'keywords' => array('map', 'product')
	));
	acf_register_block_type(array(
		'name' => 'donate',
		'title' => __('Donate'),
		'description' => 'Custom donate block',
		'render_template' => 'template-parts/blocks/donate/donate.php',
		'icon' => 'editor-paste-text',
		'keywords' => array('donate', 'product')
	));
	acf_register_block_type(array(
		'name' => 'text-banner',
		'title' => __('Text Banner'),
		'description' => 'Custom text-banner block',
		'render_template' => 'template-parts/blocks/banner/text-banner.php',
		'icon' => 'editor-paste-text',
		'keywords' => array('text-banner', 'product')
	));
	acf_register_block_type(array(
		'name' => 'articles',
		'title' => __('Articles'),
		'description' => 'Custom articles block',
		'render_template' => 'template-parts/blocks/articles/articles.php',
		'icon' => 'editor-paste-text',
		'keywords' => array('articles', 'product')
	));
	acf_register_block_type(array(
		'name' => 'question',
		'title' => __('Question'),
		'description' => 'Custom questions block',
		'render_template' => 'template-parts/blocks/question/question.php',
		'icon' => 'editor-paste-text',
		'keywords' => array('question', 'product')
	));
	acf_register_block_type(array(
		'name' => 'info',
		'title' => __('Info'),
		'description' => 'Custom info block',
		'render_template' => 'template-parts/blocks/info/info.php',
		'icon' => 'editor-paste-text',
		'keywords' => array('info', 'product')
	));
	acf_register_block_type(array(
		'name' => 'single map',
		'title' => __('Single Map'),
		'description' => 'Custom single map block',
		'render_template' => 'template-parts/blocks/map/single-map.php',
		'icon' => 'editor-paste-text',
		'keywords' => array('single map', 'product')
	));
	acf_register_block_type(array(
		'name' => 'timeline',
		'title' => __('Timeline'),
		'description' => 'Custom timeline block',
		'render_template' => 'template-parts/blocks/timeline/timeline.php',
		'icon' => 'editor-paste-text',
		'keywords' => array('timeline', 'product')
	));
	acf_register_block_type(array(
		'name' => 'photo banner',
		'title' => __('Photo Banner'),
		'description' => 'Custom photo banner block',
		'render_template' => 'template-parts/blocks/banner/photo-banner.php',
		'icon' => 'editor-paste-text',
		'keywords' => array('photo banner', 'product')
	));
	acf_register_block_type(array(
		'name' => 'location',
		'title' => __('Location'),
		'description' => 'Custom location block',
		'render_template' => 'template-parts/blocks/location/location.php',
		'icon' => 'editor-paste-text',
		'keywords' => array('location', 'product')
	));
	acf_register_block_type(array(
		'name' => 'advantages',
		'title' => __('Advantages'),
		'description' => 'Custom advantages block',
		'render_template' => 'template-parts/blocks/advantages/advantages.php',
		'icon' => 'editor-paste-text',
		'keywords' => array('advantages', 'product')
	));
	acf_register_block_type(array(
		'name' => 'application form',
		'title' => __('Application form'),
		'description' => 'Custom application form block',
		'render_template' => 'template-parts/blocks/form/application-form.php',
		'icon' => 'editor-paste-text',
		'keywords' => array('application form', 'product')
	));
	acf_register_block_type(array(
		'name' => 'blog artricles',
		'title' => __('Blog artricles'),
		'description' => 'Custom blog artricles block',
		'render_template' => 'template-parts/blocks/articles/blog-articles.php',
		'icon' => 'editor-paste-text',
		'keywords' => array('blog artricles', 'product')
	));
	acf_register_block_type(array(
		'name' => 'artricles slider',
		'title' => __('Artricles slider'),
		'description' => 'Custom artricles slider block',
		'render_template' => 'template-parts/blocks/articles/articles-slider.php',
		'icon' => 'editor-paste-text',
		'keywords' => array('artricles slider', 'product')
	));
	acf_register_block_type(array(
		'name' => 'teams',
		'title' => __('Teams'),
		'description' => 'Custom teams block',
		'render_template' => 'template-parts/blocks/teams/teams-staff.php',
		'icon' => 'editor-paste-text',
		'keywords' => array('teams', 'product')
	));
	acf_register_block_type(array(
		'name' => 'teams ambassadors',
		'title' => __('Teams ambassadors'),
		'description' => 'Custom teams ambassadors block',
		'render_template' => 'template-parts/blocks/teams/teams-ambassadors.php',
		'icon' => 'editor-paste-text',
		'keywords' => array('teams ambassadors', 'product')
	));
	acf_register_block_type(array(
		'name' => 'teams trustees',
		'title' => __('Teams trustees'),
		'description' => 'Custom teams trustees block',
		'render_template' => 'template-parts/blocks/teams/teams-trustees.php',
		'icon' => 'editor-paste-text',
		'keywords' => array('teams trustees', 'product')
	));
	acf_register_block_type(array(
		'name' => 'teams single',
		'title' => __('Teams single'),
		'description' => 'Custom teams single block',
		'render_template' => 'template-parts/blocks/teams/teams-single.php',
		'icon' => 'editor-paste-text',
		'keywords' => array('teams single', 'product')
	));
	acf_register_block_type(array(
		'name' => 'shop',
		'title' => __('Shop'),
		'description' => 'Custom shop block',
		'render_template' => 'template-parts/blocks/shop/shop.php',
		'icon' => 'editor-paste-text',
		'keywords' => array('shop', 'product')
	));
	acf_register_block_type(array(
		'name' => 'download',
		'title' => __('download'),
		'description' => 'Custom download block',
		'render_template' => 'template-parts/blocks/download/download.php',
		'icon' => 'editor-paste-text',
		'keywords' => array('download', 'product')
	));
	acf_register_block_type(array(
		'name' => 'strategy',
		'title' => __('Strategy'),
		'description' => 'Custom strategy block',
		'render_template' => 'template-parts/blocks/strategy/strategy.php',
		'icon' => 'editor-paste-text',
		'keywords' => array('strategy', 'product')
	));
	acf_register_block_type(array(
		'name' => 'introduction',
		'title' => __('Introduction'),
		'description' => 'Custom introduction block',
		'render_template' => 'template-parts/blocks/introduction/introduction.php',
		'icon' => 'editor-paste-text',
		'keywords' => array('introduction', 'product')
	));
	acf_register_block_type(array(
		'name' => 'donation form',
		'title' => __('Donation form'),
		'description' => 'Custom donation form block',
		'render_template' => 'template-parts/blocks/donate/donation-form.php',
		'icon' => 'editor-paste-text',
		'keywords' => array('donation form', 'product')
	));
}


if(function_exists('acf_register_block_type')) {
	add_action('acf/init', 'register_acf_block_types');
}

add_filter('wp_nav_menu_objects', 'my_wp_nav_menu_objects', 10, 2);

function my_wp_nav_menu_objects( $items, $args ) {

	foreach( $items as $item ) {

		$icon = get_field('menu_item_image', $item);
		$menu = get_field('menu_item', $item);

		$args = compact('icon', 'menu');

		// if ($menu) {
		//     get_template_part('template-parts/menus/nav-menu-objects', null, $args);
    //     }

		
		if ($menu) {

			$html .= '<div class="menu-dropdown">
			<div class="container">
				<ul class="dropdown-list">';
				if ($icon) {
					$html .= '<li class="dropdown-list__item">
					<img class="dropdown-list__img" src="' . $icon . '" alt="Test"/>
				</li>';
				}
			foreach ((array) $menu as $menuItem) {
			$html .= '<li class="dropdown-list__item">
				<a class="dropdown-list__link" href="'. $menuItem['menu_item_link'] .'">
					<span class="dropdown-list__title">' . $menuItem['menu_item_title'] .'
						<svg width="12" height="7" viewBox="0 0 12 7" fill="none" xmlns="http://www.w3.org/2000/svg">
						<path fill-rule="evenodd" clip-rule="evenodd" d="M1.78333 0.666687L6 4.50002L10.2167 0.666687L11.5 1.83335L6 6.83335L0.5 1.83335L1.78333 0.666687Z" fill="#979797"/>
						</svg>
					</span>
					<p class="dropdown-list__descr">' . $menuItem['menu_item_descr'] . '</p>';
					if ($menuItem['menu_item_subimage']) {
						$html .= '<img class="dropdown-list__subimg" src="' . $menuItem['menu_item_subimage'] .'"  alt="' . $menuItem['menu_item_title'] . '"/>
					</a>
				</li>';

					}
			}
			$html .= '</ul></div></div>';
			 $item->title .= $html;
		}
        

	}
	return $items;

}


function titleColor($name, $index) {
	$name = preg_split("/\s+/", $name);
	$name[$index] = "<span>" . $name[$index] . "</span>";
	$name = join(" ", $name);
	echo $name;
}
